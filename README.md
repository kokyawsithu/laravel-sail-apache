# Laravel Sail Apache

Docker configs for using Sail with Apache web server

## Getting started

Copy config files into your laravel project root folder. Folder structure will be like following.

```
Your Project Folder
|__docker
|  |__8.1-apache
|    |__Dockerfile
|     |__php.ini
|     |__vhost.conf
|__docker-compose.yml
```

Run 'sail build --no-cache'

Run 'sail up -d'

## Usage

Recommended for Development enviroment only. However, you are free to use this configs at your own risks for any enviroment. 
